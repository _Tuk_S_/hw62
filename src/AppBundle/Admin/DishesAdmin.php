<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class DishesAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('price')
            ->add('descriptions')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('name')
            ->add('price')
            ->add('descriptions')
            ->add('restaurants', EntityType::class, [
                'class' => 'AppBundle\Entity\Restaurants'
            ])
//            ->add('user', EntityType::class, [
//                'class' => 'AppBundle\Entity\User'
//            ])
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }


    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', TextType::class)
            ->add('price', MoneyType::class)
            ->add('descriptions', TextType::class)
            ->add('restaurants', Entitytype::class, [
                'class' => 'AppBundle\Entity\Restaurants'
            ])
//            ->add('user', EntityType::class, [
//                'class' => 'AppBundle\Entity\User'
//            ])
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('name')
            ->add('price')
            ->add('descriptions')
            ->add('restaurants', Entitytype::class, [
                'class' => 'AppBundle\Entity\Restaurants'
            ])
//            ->add('user', EntityType::class, [
//                'class' => 'AppBundle\Entity\User'
//            ])
        ;
    }
}
