<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LoadArticleData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user
            ->setUsername('Satybaldiev')
            ->setRoles(["ROLE_ADMIN"])
            ->setEmail('original@some.com')
            ->setEnabled(true)
            ->setAddress('Bishkek')
            ->setPhone('0555400077')
            ->setName('Turganbay');

        $encoder = $this->container->get('security.password_encoder');
        $password = $encoder->encodePassword($user, '123321');
        $user->setPassword($password);

        $manager->persist($user);
        $manager->flush();
    }


    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}