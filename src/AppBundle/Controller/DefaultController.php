<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $session = $this->get('session');
        $session_id = $session->getId();

        if (!$session->has($session_id)) {
            $session->set($session_id, 0);
        }

        $restaurants = $this->getDoctrine()
            ->getRepository('AppBundle:Restaurants')
            ->findAll();
        return $this->render('default/index.html.twig', [
            'restaurants' => $restaurants,
        ]);
    }


    /**
     * Creates a new Dishes entity.
     * @Route("/dishes/{id}", requirements={"id": "\d+"})
     * @Method({"GET", "POST"})
     * @param $id integer
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function dishesShowAction(Request $request, int $id)
    {
        $session = $this->get('session');

        $em = $this->getDoctrine()->getManager();
        $dishes = $em->getRepository('AppBundle:Dishes')->findAll();

        $result = [];

        foreach ($dishes as $key => $val) {
            if ($val->getRestaurants()->getId() != $id) {
                continue;
            }
            $result[] = [
                'id' => $val->getId(),
                'name' => $val->getName(),
                'price' => $val->getPrice()
            ];
        }

        $restaurants = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:Restaurants')
            ->findAll();
        $restaurant_id = [];

        foreach ($restaurants as $value) {
            if ($value->getId() != $id) {
                continue;
            }
            $restaurant_id[] = [
                'id' => $value->getId(),
                'descriptions' => $value->getDescriptions(),
                'title' => $value->getTitle()
            ];
        }

        $dishes_array = [];
        foreach ($_SESSION as $key => $value) {
            if ($key == $session->getId()) {
                $dishes_array[] = $value;
            }
        }



        return $this->render('default/dishes_show.html.twig', [
            'dishes' => $result,
            'restaurant' => $restaurant_id,
            'dishes_array' => $dishes_array,
            'id' => $session->getId(),
            'restaurant_id' => $id,
        ]);
    }


    /**
     * @Route("/basket/{id}", requirements={"id": "\d+"})
     * @Method({"GET", "POST"})
     * @param $id integer
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function basketAction(Request $request, int $id)
    {
        $session = $this->get('session');
        $session_id = $session->getId();

        if (!$session->has($session_id)) {
            $session->set($session_id, 1);
        } else {
            $counter = $session->get($session_id);
            $session->set($session_id, $counter + 1);
        }

        $count = $session->get($session_id);

        $to_basket = [];
        foreach ($request as $value) {
            foreach ($value as $key => $val) {
                $to_basket[$key] = $val;
            }
        }

        $em = $this->getDoctrine()->getManager();
        $dish = $em->getRepository('AppBundle:Dishes')
            ->find($to_basket['to_basket_id']);

        $_SESSION[$session_id][$count] = $dish;

        return $this->redirectToRoute('app_default_dishesshow', ['id' => $id]);
    }
}
