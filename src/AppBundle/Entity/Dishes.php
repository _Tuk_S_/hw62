<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dishes
 *
 * @ORM\Table(name="dishes")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DishesRepository")
 */
class Dishes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=127)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal")
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="descriptions", type="text")
     */
    private $descriptions;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Restaurants", inversedBy="dishes")
     */
    private $restaurants;

    public function setRestaurants(Restaurants $restaurants)
    {
        $this->restaurants = $restaurants;
    }

    public function getRestaurants()
    {
        return $this->restaurants;
    }

//    /**
//     * @ORM\ManyToOne(targetEntity="User", inversedBy="dishes")
//     */
//    private $user;
//
//    public function setUser(User $user)
//    {
//        $this->user = $user;
//    }
//
//    public function getUser()
//    {
//        return $this->user;
//    }

    public function __toString()
    {
        return $this->name ?: '';
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Dishes
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Dishes
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set descriptions
     *
     * @param string $descriptions
     *
     * @return Dishes
     */
    public function setDescriptions($descriptions)
    {
        $this->descriptions = $descriptions;

        return $this;
    }

    /**
     * Get descriptions
     *
     * @return string
     */
    public function getDescriptions()
    {
        return $this->descriptions;
    }
}
